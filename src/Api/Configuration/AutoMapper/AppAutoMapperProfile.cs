using Application.Dto;
using AutoMapper;
using Domain.Model;

namespace API.Configuration.AutoMapper
{
    public class AppAutoMapperProfile : Profile
    {
        public AppAutoMapperProfile()
        {
            // Domain To CQRS



            // Domain To Dto
            CreateMap<AppUser, AppUserDto>();



            // Request To CQRS


        }
    }
}