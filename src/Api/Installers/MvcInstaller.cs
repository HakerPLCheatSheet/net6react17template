using System.Reflection;
using System.Text.Json.Serialization;
using API.Middlewares;
using Application.Configuration.Database;
using FluentValidation.AspNetCore;
using MediatR;

namespace API.Installers
{
    public class MvcInstaller : IInstaller
    {
        public void InstallServices(IConfiguration configuration, IServiceCollection services)
        {
            services.AddControllers(options =>
                {
                    options.Filters.Add<ValidationFilter>();
                })
                .AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore)
                .AddJsonOptions(options => options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()))
                .AddFluentValidation(mcvConfiguration =>
                    mcvConfiguration.RegisterValidatorsFromAssemblyContaining<Startup>());

            services.AddMediatR(Assembly.GetExecutingAssembly(), typeof(IAppDbContext).Assembly);
            services.AddAutoMapper(typeof(IAppDbContext), typeof(Startup));
        }
    }
}