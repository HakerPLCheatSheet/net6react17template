using AutoMapper;
using Domain.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace API.Controllers
{
    [Route("[controller]/[action]")]
    public class ApiController : ControllerBase
    {
        private IMediator _mediator;
        private IMapper _mapper;

        protected IMapper Mapper => _mapper ??= HttpContext.RequestServices.GetService<IMapper>();
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();


        protected IActionResult ReturnErrorMessage(Exception ex)
        {
            var baseException = ex as BaseException;

            if (baseException != null)
            {
                return StatusCode(400, baseException.Message);
            }

            return StatusCode(500, "Internal server error");
        }

        protected void LogError(Exception ex, string className, string methodName)
        {
            string message = ex.Message;
            var baseException = ex as BaseException;

            if (baseException != null && baseException.InnerException != null)
            {
                message = baseException.InnerException.Message;
            }

            Log.Error($"Something went wrong inside {className}->{methodName} action. Message: {message}");
        }
    }
}