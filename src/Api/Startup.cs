﻿using API.Data;
using Serilog;
using API.Installers;
using API.Configuration;
using Autofac;
using Application.Configuration.Database;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.InstallServicesInAssembly(Configuration);
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder
                .RegisterType<ApplicationDbContext>()
                .As<IAppDbContext>()
                .InstancePerLifetimeScope();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();

                var swaggerConfiguration = new SwaggerConfiguration();
                Configuration.GetSection(nameof(SwaggerConfiguration)).Bind(swaggerConfiguration);

                app.UseSwagger(options =>
                {
                    options.RouteTemplate = swaggerConfiguration.JsonRoute;
                });

                app.UseSwaggerUI(options =>
                {
                    options.SwaggerEndpoint(swaggerConfiguration.UiEndpoint, swaggerConfiguration.Description);
                    options.RoutePrefix = string.Empty;
                });
            }

            app.UseSerilogRequestLogging();
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseCors(x => x
                .AllowAnyMethod()
                .AllowAnyHeader()
                .SetIsOriginAllowed(origin => true) // allow any origin
                .AllowCredentials()); // allow credentials

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
