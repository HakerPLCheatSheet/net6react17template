using Application.Configuration.CQRS;
using Domain.Model.Authentication;

namespace Application.Identity.Commands.RefreshToken
{
    public class RefreshTokenCommand : IQuery<AuthenticationResult>
    {
        public string Token { get; }
        public string RefreshToken { get; }

        public RefreshTokenCommand(string token, string refreshToken)
        {
            Token = token;
            RefreshToken = refreshToken;
        }
    }
}