using Application.Configuration.CQRS;
using Domain.Model.Authentication;

namespace Application.Identity.Commands.UserLogin
{
    public class UserLoginCommand : IQuery<AuthenticationResult>
    {
        public string Email { get; }
        public string Password { get; }

        public UserLoginCommand(string email, string password)
        {
            Email = email;
            Password = password;
        }
    }
}