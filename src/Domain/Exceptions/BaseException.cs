﻿using System;

namespace Domain.Exceptions
{
    public abstract class BaseException : Exception
    {
        public override string Message => base.Message;

        public BaseException(Exception innerException = null) : base(string.Empty, innerException) { }
    }
}
